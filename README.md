# online fashion store with React+MUI+k8s




export INGRESS_PORT=$(kubectl -n istio-system get service istio-ingressgateway -o jsonpath='{.spec.ports[?(@.name=="http2")].nodePort}')
export SECURE_INGRESS_PORT=$(kubectl -n istio-system get service istio-ingressgateway -o jsonpath='{.spec.ports[?(@.name=="https")].nodePort}')
export INGRESS_HOST=$(kubectl get po -l istio=ingressgateway -n istio-system -o jsonpath='{.items[0].status.hostIP}')
export GATEWAY_URL=NODEIP:$INGRESS_PORT
echo "$GATEWAY_URL"
echo "http://$GATEWAY_URL/


# health check endpoint
# https://kubernetes.io/ja/docs/tasks/configure-pod-container/configure-liveness-readiness-startup-probes/



kubectl apply -f samples/addons
kubectl rollout status deployment/kiali -n istio-system
# kubectl get svc -n istio-system
kubectl port-forward service/kiali -n istio-system --address 0.0.0.0 8889:20001
http://34.146.130.74:8889/



how to run 

developing in local
in each directory
npm start
and redis-histories is NOT automatically deployed in histories/app.js/createClient
deploy with docker manually
docker run --name histories-redis -p 6379:6379 -d redis
docker container ls -a

run with docker-compose
use nginx reverse proxy for visiting each RESTful API
in root directory(react-onlinestore-with-mui-k8s) 
docker-compose up --build

run with kubernetes w/o istio/dapr
kubectl apply -f deploy-k8s.yml

run with kubernetes with istio and istio-gateway
after deploying Istio...
kubectl apply -f gateway-k8s-istio.yml
then get and change REACT_APP_SERVICE_API_URLs in deploy-k8s-with-istio-gtw.yml

export INGRESS_PORT=$(kubectl -n istio-system get service istio-ingressgateway -o jsonpath='{.spec.ports[?(@.name=="http2")].nodePort}')
export SECURE_INGRESS_PORT=$(kubectl -n istio-system get service istio-ingressgateway -o jsonpath='{.spec.ports[?(@.name=="https")].nodePort}')
export INGRESS_HOST=$(kubectl get po -l istio=ingressgateway -n istio-system -o jsonpath='{.items[0].status.hostIP}')
export GATEWAY_URL=NODEIP:$INGRESS_PORT
echo "$GATEWAY_URL"
echo "http://$GATEWAY_URL/

kubectl apply -f deploy-k8s-with-istio-gtw.yml


when initiate rancher environent
chmod 777 cleanup.sh
./cleanup.sh




