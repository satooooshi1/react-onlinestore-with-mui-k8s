// https://www.npmjs.com/package/@faker-js/faker

const { faker } = require('@faker-js/faker');
const randomName = faker.name.findName(); // Rowan Nikolaus
const randomEmail = faker.internet.email(); // Kassandra.Haley@erich.biz
const randomCard = faker.helpers.createCard(); // random contact card containing many properties
//console.log(randomCard)


const productIds=[
    'prod_8XO3wpd6vOwYAz',
    'prod_bO6J5a821EoEjp',
    'prod_Op1YoVEALgwXLv',
    'prod_RqEv5xzL10wZz4',
    'prod_AYrQlWdpJxwnbR',
    'prod_bO6J5a82g8oEjp'
]

let comments=['素晴らしい入門書', '感慨ぶかい', '不朽の名作をこのお値段で観られるなんて！', '集中して楽しめると思います', '懐かしい', 'おもしろい', '気持ちよく読める新訳']

let res=[];
for(let i=0;i<20;i++){
    let item= {
        review_id: 'review_'+faker.datatype.uuid(),
        title: faker.lorem.words(),
        stars: faker.datatype.number({ 'min': 1, 'max': 5 }),
        date:faker.date.between('2021-10-01', '2022-03-22').toString(),
        content: faker.lorem.sentences(),
        product_id: faker.random.arrayElement(productIds)
      }
    res.push(item)
}
console.log(res)