const express = require('express')
const cors = require("cors");
const app = express()
const host='0.0.0.0'
const port = 3003


let corsOptions = {
  origin: "http://localhost:3000"
};
app.use(cors(corsOptions));
// parse requests of content-type - application/json
app.use(express.json());
// parse requests of content-type - application/x-www-form-urlencoded
app.use(express.urlencoded({ extended: true }));


//ミドルウェア関数をロード
// https://expressjs.com/ja/guide/writing-middleware.html
var requestTime = function (req, res, next) {
  // formate new Date()
  // https://www.javadrive.jp/javascript/date_class/index2.html#section1
  console.log(`request time: ${new Date().toLocaleString('ja-JP')}`)
  next()// next() 関数を呼び出して、スタック内の次のミドルウェア関数へのリクエストに移ります。
}
// ミドルウェアのロード。最初にロードされたミドルウェア関数が常に最初に実行されます。
app.use(requestTime)

app.get('/api/reviews/hello', function (req, res) {
  return res.send({
    data:'hello reviews'
  })
})

function getReviewsByProductId(productId) {
  console.log(`respond reviews of productId ${productId}`)
  console.log(reviews.filter(
    review => review.product_id === productId
  ))
  return reviews.filter(
    review => review.product_id === productId
  )
}

app.get('/api/reviews/:productId', function (req, res) {
  console.log(`get review of prouct: ${req.params.productId}`)
  return res.send(getReviewsByProductId(req.params.productId))
})

app.get('/api/reviews/add/:productId/:content', function (req, res) {
  console.log(req.params)
})


app.listen(port,host, () => {
  console.log(`review API listening on ${host}:${port}`)
})





let reviews=[
  {
    review_id: 'review_6cf69945-1c3d-4576-9d35-52107f9d3a56',
    title: 'consectetur minima qui',
    stars: 5,
    date: 'Tue Nov 09 2021 09:40:28 GMT+0900 (日本標準時)',
    content: 'Eveniet nobis aut possimus quasi dicta aut non voluptas. Ut cum cumque et facilis debitis aperiam hic. Adipisci id distinctio cumque ullam. Totam quidem porro autem autem fugiat quod unde et. Sunt velit quis.',
    product_id: 'prod_RqEv5xzL10wZz4'
  },
  {
    review_id: 'review_7d05650c-d611-4e95-98a9-351e801bff0c',
    title: 'quidem ad voluptas',
    stars: 2,
    date: 'Fri Oct 15 2021 12:07:58 GMT+0900 (日本標準時)',
    content: 'Et magnam temporibus id repellendus cupiditate ut. Nemo et non. Ea nam quisquam impedit et ab ad commodi. Atque ea voluptas suscipit eum. Autem aut odit.',
    product_id: 'prod_8XO3wpd6vOwYAz'
  },
  {
    review_id: 'review_d91623dc-dc9d-4c1b-a06f-a4f16677cc4e',
    title: 'a officia cupiditate',
    stars: 3,
    date: 'Wed Dec 29 2021 01:21:41 GMT+0900 (日本標準時)',
    content: 'Excepturi unde mollitia vitae et sit quia debitis deleniti dolores. Aut atque officia dolor iste. Est qui sed quo quia illo enim. Corporis similique sit soluta. Omnis omnis sunt ullam suscipit atque id assumenda corporis.',
    product_id: 'prod_8XO3wpd6vOwYAz'
  },
  {
    review_id: 'review_1ea73661-fcbc-438e-b75d-87791e8d5930',
    title: 'animi amet corrupti',
    stars: 5,
    date: 'Thu Dec 02 2021 23:32:22 GMT+0900 (日本標準時)',
    content: 'Ratione quo quod consequatur. Est temporibus voluptatem explicabo est quibusdam rerum ut voluptatem quae. Eaque omnis nisi assumenda beatae voluptatum rerum laudantium. Porro ipsa doloremque ut suscipit voluptatem exercitationem. Omnis vel quia et.',
    product_id: 'prod_Op1YoVEALgwXLv'
  },
  {
    review_id: 'review_1d06877d-bd81-44ad-ab02-84b24130eac8',
    title: 'omnis aperiam non',
    stars: 1,
    date: 'Sun Oct 03 2021 06:40:04 GMT+0900 (日本標準時)',
    content: 'Dolorem consequatur quia dolores aliquam aliquid rerum quia dolorum. Sequi necessitatibus et quidem quaerat consequatur qui error blanditiis. Non ullam voluptas. Ratione aliquid dolor. Quia et et aliquid doloremque consequatur. Laborum aut dolorum.',
    product_id: 'prod_8XO3wpd6vOwYAz'
  },
  {
    review_id: 'review_2194611f-f576-46f4-9d52-62e1d91503b3',
    title: 'et quo fuga',
    stars: 3,
    date: 'Wed Feb 09 2022 09:39:09 GMT+0900 (日本標準時)',
    content: 'Optio dolorem voluptatem voluptas nobis veritatis odio possimus. Blanditiis voluptatem quis. Dolores unde modi consequatur numquam voluptas culpa. Rem non commodi autem ut culpa odio voluptatem.',
    product_id: 'prod_8XO3wpd6vOwYAz'
  },
  {
    review_id: 'review_ad8cd8a8-f57e-4996-86d8-83df7ad211bd',
    title: 'qui qui porro',
    stars: 5,
    date: 'Sat Oct 30 2021 08:36:08 GMT+0900 (日本標準時)',
    content: 'Cumque doloribus quis dolores error omnis qui aut blanditiis quibusdam. Quos sit assumenda numquam. Assumenda fugit molestiae vel voluptas molestiae nobis incidunt.',
    product_id: 'prod_RqEv5xzL10wZz4'
  },
  {
    review_id: 'review_310b432b-55e3-4940-a467-f23ff87d3056',
    title: 'magni ut similique',
    stars: 2,
    date: 'Tue Feb 01 2022 11:44:45 GMT+0900 (日本標準時)',
    content: 'Est sint ut consectetur maxime. Quaerat ducimus veritatis. In ut in nam ullam nemo quo aut.',
    product_id: 'prod_bO6J5a821EoEjp'
  },
  {
    review_id: 'review_0a65277e-fbf0-4be6-8627-705e20f0b15e',
    title: 'veritatis quam rerum',
    stars: 4,
    date: 'Sun Jan 23 2022 13:21:23 GMT+0900 (日本標準時)',
    content: 'Ut fuga ut est voluptatem laborum corrupti. Est aut rerum veritatis tenetur est. Asperiores soluta aut ut ipsam fugit. Velit cum in asperiores beatae aut eligendi adipisci.',
    product_id: 'prod_AYrQlWdpJxwnbR'
  },
  {
    review_id: 'review_b36326c0-f360-4f27-9616-b44590589efa',
    title: 'ex nemo totam',
    stars: 1,
    date: 'Tue Jan 18 2022 22:17:59 GMT+0900 (日本標準時)',
    content: 'Culpa explicabo inventore officiis voluptate adipisci rerum. Ea qui quia. Dolore ducimus laudantium illo et nam. Consequatur rerum fugit hic id iste est voluptas.',
    product_id: 'prod_bO6J5a821EoEjp'
  },
  {
    review_id: 'review_d9485c54-99c5-4721-953a-d28ff167be59',
    title: 'ullam quibusdam rem',
    stars: 4,
    date: 'Thu Nov 11 2021 01:19:08 GMT+0900 (日本標準時)',
    content: 'Rerum suscipit iste. Distinctio placeat quia. Magni ut assumenda magni. Totam neque porro dicta maxime.',
    product_id: 'prod_8XO3wpd6vOwYAz'
  },
  {
    review_id: 'review_11205dbf-308e-4733-8a2a-af9ebaf5165e',
    title: 'doloremque ab sed',
    stars: 5,
    date: 'Thu Feb 03 2022 20:10:37 GMT+0900 (日本標準時)',
    content: 'Molestiae eaque eius. Atque quia odit dolores voluptas et pariatur ullam labore non. Totam rerum earum veniam fugiat. Amet autem ipsum blanditiis ut commodi soluta eveniet et. Et fugiat voluptatem omnis ut non ut soluta harum pariatur.',
    product_id: 'prod_RqEv5xzL10wZz4'
  },
  {
    review_id: 'review_606b4348-f423-4bac-8bbf-dd86f139d922',
    title: 'consectetur et dolore',
    stars: 2,
    date: 'Tue Mar 08 2022 11:39:28 GMT+0900 (日本標準時)',
    content: 'Sit amet quis ex autem. Iure officiis quaerat maiores aut.',
    product_id: 'prod_Op1YoVEALgwXLv'
  },
  {
    review_id: 'review_2d4f9cdd-52f6-4bd9-a338-e2628e854860',
    title: 'nesciunt sit deleniti',
    stars: 1,
    date: 'Fri Dec 17 2021 04:50:35 GMT+0900 (日本標準時)',
    content: 'Rem aut adipisci quia. Sint quae a.',
    product_id: 'prod_bO6J5a82g8oEjp'
  },
  {
    review_id: 'review_be29aa57-f6be-49e7-9c62-666b5112bf3b',
    title: 'aut nihil cupiditate',
    stars: 1,
    date: 'Mon Mar 14 2022 18:03:55 GMT+0900 (日本標準時)',
    content: 'Sit dolorem possimus perspiciatis. Provident sequi nobis et commodi a deleniti dolorum est.',
    product_id: 'prod_Op1YoVEALgwXLv'
  },
  {
    review_id: 'review_4a5d8ff8-4c59-464f-a6bd-0039e91ab91e',
    title: 'dolores nam aut',
    stars: 2,
    date: 'Thu Jan 20 2022 07:11:43 GMT+0900 (日本標準時)',
    content: 'Qui voluptas dolores. Quisquam suscipit et expedita exercitationem. Et et velit ut recusandae vero. Rerum recusandae at dolor aperiam. Rerum qui sunt aliquid expedita porro quis.',
    product_id: 'prod_bO6J5a82g8oEjp'
  },
  {
    review_id: 'review_2b32a6ff-6316-4c20-a3e6-186f72d457f6',
    title: 'qui aliquam est',
    stars: 3,
    date: 'Fri Mar 04 2022 01:41:09 GMT+0900 (日本標準時)',
    content: 'Ullam saepe amet soluta fuga eligendi quisquam hic ea. Ducimus quos aut sunt. Perferendis aut dolor eum expedita eveniet. Molestiae sint necessitatibus.',
    product_id: 'prod_bO6J5a82g8oEjp'
  },
  {
    review_id: 'review_9568e5e9-bfdf-425f-811e-9c7737811062',
    title: 'ea totam velit',
    stars: 3,
    date: 'Sun Jan 23 2022 02:12:04 GMT+0900 (日本標準時)',
    content: 'Incidunt quaerat ut et placeat possimus. Voluptate nostrum officiis. Placeat et temporibus facere. Voluptatibus libero esse cum aliquam tenetur qui quaerat enim aliquam.',
    product_id: 'prod_RqEv5xzL10wZz4'
  },
  {
    review_id: 'review_57f3dd9a-451f-4033-b675-608796650942',
    title: 'modi ut ipsa',
    stars: 4,
    date: 'Sat Dec 18 2021 16:18:19 GMT+0900 (日本標準時)',
    content: 'Ut consequatur nobis doloribus odio dolores. Aut labore velit.',
    product_id: 'prod_8XO3wpd6vOwYAz'
  },
  {
    review_id: 'review_5049fa72-be6e-4ffe-8602-b154c1912202',
    title: 'reiciendis blanditiis facere',
    stars: 3,
    date: 'Fri Mar 18 2022 05:00:42 GMT+0900 (日本標準時)',
    content: 'Delectus magnam consectetur iusto magni ut voluptates et ea. Veritatis corrupti voluptatem voluptas et nobis molestiae molestiae. Earum dolor distinctio sint. Amet voluptatibus molestiae et nesciunt numquam sed doloribus repellendus aspernatur.',
    product_id: 'prod_Op1YoVEALgwXLv'
  }
]