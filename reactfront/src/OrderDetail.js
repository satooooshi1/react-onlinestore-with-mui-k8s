import React, { useState, useEffect } from 'react'
import { styled } from '@mui/material/styles';
import {Box,Button, Paper, Grid, Rating} from '@mui/material';

import Loading from './Loading'

import {commerce} from './lib/commerce'

const Item = styled(Paper)(({ theme }) => ({
  backgroundColor: theme.palette.mode === 'dark' ? '#1A2027' : '#fff',
  ...theme.typography.subtitle,
  padding: theme.spacing(1),
  textAlign: 'center',
  color: theme.palette.text.secondary,
}));


const dateFormatter = (data) => {
  let myDate = new Date(data*1000)
  console.log(myDate.getFullYear() + '-' +('0' + (myDate.getMonth()+1)).slice(-2)+ '-' +  ('0' + myDate.getDate()).slice(-2) + ' '+myDate.getHours()+ ':'+('0' + (myDate.getMinutes())).slice(-2)+ ':'+myDate.getSeconds())
  return myDate.getFullYear() + '-' +('0' + (myDate.getMonth()+1)).slice(-2)+ '-' +  ('0' + myDate.getDate()).slice(-2) + ' '+myDate.getHours()+ ':'+('0' + (myDate.getMinutes())).slice(-2)+ ':'+myDate.getSeconds()
}


export default function OrderDetail() {

  const [loading, setLoading] = React.useState(true)
  const [order, setOrder] = useState({})

  useEffect(() => {
    const id = window.location.pathname.split("/")
    fetchOrder(id[3])
  }, [])


  const fetchOrder = (orderId) => {
    setLoading(true)
    console.log(orderId)
    let customerId=null
    customerId=localStorage.getItem('token')
    const url = new URL(
      "https://api.chec.io/v1/customers/"+customerId+"/orders/"+orderId
    );

    let headers = {
      "X-Authorization": process.env.REACT_APP_CHEC_SECRET_KEY,
      "Accept": "application/json",
      "Content-Type": "application/json",
    };

    fetch(url, {
      method: "GET",
      headers: headers,
    })
    .then(response => response.json())
    .then(json => {
        console.log(json)
        setOrder(json)
        setLoading(false)
    });

  };

  if(order?.id==undefined || loading )return <Loading/>
  return (
    <Paper elevation={0} sx={{
      flexGrow: 1,
      backgroundColor: (theme) => theme.palette.mode === 'dark' ? '#1A2027' : '#fff',
    }} >
      <Grid container spacing={0} >

        <Grid item md={12} >
          <Item elevation={0}>注文の詳細: {order?.id} </Item>
        </Grid>

        <Grid item md={12} >
          <Item elevation={0}>{dateFormatter(order.created)} の注文</Item>
        </Grid>

        <Grid item md={12} >
          <Item elevation={0}>注文した商品</Item>
        </Grid>

        <Grid item xs={6} md={12}>
        <Grid container justifyContent="center" spacing={2}>
        {order.order.line_items.map((item,idx) => (
        <Grid elevation={0}　item key={idx}>
        <Item　elevation={0}　>
          <OrderedItem item={item}/>
        </Item>
        </Grid>
        ))}
        </Grid>
        </Grid>

        <Grid item md={12} >
          <Item elevation={0} sx={{ height:30,}}>合計: {order.order_value.formatted_with_code}</Item>
        </Grid>

      </Grid>
    </Paper>
  );
}




function OrderedItem({item}) {
  return (
    <Paper elevation={0} sx={{
      flexGrow: 1,
      backgroundColor: (theme) => theme.palette.mode === 'dark' ? '#1A2027' : '#fff',
    }} >
      <Grid container spacing={0} justifyContent="center" alignItems="center">

        <Grid item md={4} >
          <Item elevation={0} ><img width="180" height="180"  src={item.image.url} alt="海の写真" title="空と海"/></Item>
        </Grid>

        <Grid item md={4} >
          <Item elevation={0} >
          <Grid container spacing={1} >
          <Grid item xs={12}>
            <Item elevation={0}>{item.name}</Item>
          </Grid>
          <Grid item xs={12}>
            <Item elevation={0}>ブラック / M</Item>
          </Grid>
          <Grid item xs={12}>
            <Item elevation={0} >{item.price.formatted_with_code}</Item>
          </Grid>
          <Grid item xs={12}>
          </Grid>
          </Grid>
          </Item>
        </Grid>

        
        <Grid item md={4} >
        <Grid container spacing={0} >
          <Item elevation={0}><Button href={"/products/"+item.product_id} >商品の詳細</Button></Item>
        </Grid>
        </Grid>
        

      </Grid>
    </Paper>
  );
}