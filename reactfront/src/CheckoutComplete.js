import { useLocation, NavLink } from "react-router-dom";
import {commerce} from './lib/commerce'
import { Paper, Grid, Button, Box, Typography } from '@mui/material'

export default function CheckoutComplete() {

    let location = useLocation()
    console.log(location)
    let loginToken=location.pathname.substring(location.pathname.lastIndexOf('/') + 1)
    console.log(loginToken)
    commerce.customer.getToken(loginToken).then((jwt) => {
      console.log(jwt); 
      //commerce.customer.getOrders().then((orders) => console.log(orders));
    }); //the customer is now automatically saved in local session storage,
    // here should wait for the uppper commerce api to be returned
    commerce.customer.about()
    //commerce.customer.about().then((customer) => console.log(customer))
    //console.log(commerce.customer.isLoggedIn());
    //commerce.customer.logout();
    

    return (
        <Box sx={{ display: 'flex', justifyContent:'center' }}>
          <Typography variant="title">ありがとうございます、注文を完了しました!
          <Button type="button" variant="outlined" href="/" >フロントページへいく</Button>
          </Typography>
        </Box>
    );
  }