
export const discoverService = async (svcname) => {
    console.log(svcname)
    const url = new URL(
      `http://34.146.130.74:3010/api/svc/serviceDiscovery/${svcname}`
    );
    let headers = {
      "Accept": "application/json",
      "Content-Type": "application/json",
    };
    await fetch(url, {
      method: "GET",
      headers: headers,
    })
    .then(response => response.json())
    .then(json => {
        console.log(json)
    })
    .catch( err => {
      console.log(err)
    })
}

export async function serviceDiscoverya(svcname) {
    console.log(svcname)
    const url = new URL(
     // `http://34.146.130.74:3010/api/svc/serviceDiscovery/${svcname}`
      process.env.REACT_APP_REVIEWS_API_URL+`/api/reviews/`+svcname
    );
    let headers = {
      "X-Authorization": process.env.REACT_APP_CHEC_SECRET_KEY,
      "Accept": "application/json",
      "Content-Type": "application/json",
    };

    const res = await fetch(url, {
      method: "GET",
      headers: headers,
    })
    //return res // return promise
    return res.json() // return body
  }

// https://docs.dapr.io/developing-applications/building-blocks/service-invocation/howto-invoke-discover-services/


export async function showDaprInfo(){
  // Dapr
  // These ports are injected automatically into the container.
  const daprPort = process.env.DAPR_HTTP_PORT; 
  const daprGRPCPort = process.env.DAPR_GRPC_PORT;
  const svcname = `reviews`;
  const svcUrl = `http://localhost:${daprPort}/v1.0/invoke/${svcname}/method/api/reviews/hello`;// metadata:name: statestore
  console.log("DAPR_HTTP_PORT: " + daprPort);
  console.log("DAPR_GRPC_PORT: " + daprGRPCPort);


  await fetch(`http://localhost:${daprPort}/v1.0/invoke/reviews/method/api/reviews/hello`, {
    method: "GET",
    headers: {
      "Accept": "application/json",
      "Content-Type": "application/json",
    },
  })
  .then(response => response.json())
  .then(json => {
      console.log(json)
  })
  .catch( err => {
    console.log(err)
  })


  await fetch(`http://localhost:${daprPort}/v1.0/invoke/histories/method/api/histories/hello`, {
    method: "GET",
    headers: {
      "Accept": "application/json",
      "Content-Type": "application/json",
    },
  })
  .then(response => response.json())
  .then(json => {
      console.log(json)
  })
  .catch( err => {
    console.log(err)
  })

  await fetch(`http://localhost:${daprPort}/v1.0/invoke/customers/method/api/customers/hello`, {
    method: "GET",
    headers: {
      "Accept": "application/json",
      "Content-Type": "application/json",
    },
  })
  .then(response => response.json())
  .then(json => {
      console.log(json)
  })
  .catch( err => {
    console.log(err)
  })

  console.log({SVC_NAME: svcname, SVC_URL: svcUrl, DAPR_HTTP_PORT: daprPort, DAPR_GRPC_PORT: daprGRPCPort })
  return {SVC_NAME: svcname, SVC_URL: svcUrl, DAPR_HTTP_PORT: daprPort, DAPR_GRPC_PORT: daprGRPCPort }

}