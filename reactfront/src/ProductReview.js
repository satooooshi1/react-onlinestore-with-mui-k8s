import * as React from 'react';
import { useEffect } from "react";
import { Card, Grid } from '@mui/material'
import CardHeader from '@mui/material/CardHeader';
import CardContent from '@mui/material/CardContent';
import Avatar from '@mui/material/Avatar';
import Typography from '@mui/material/Typography';
import { red } from '@mui/material/colors';
import Rating from '@mui/material/Rating';

import Loading from './Loading'

import { discoverService, serviceDiscoverya, showDaprInfo } from './apis/serviceResourceApis'
import { main } from './apis/daprApis'

export default function ProductReview({productId}) {

  const [reviews, setReviews] = React.useState([])

  useEffect(() => {
    //const id = window.location.pathname.split("/")
    //fetchOrder(id[3])
    fetchReviews(productId)
  }, [])


  const fetchReviews = async () => {
    await showDaprInfo()
    //main()
    const resu= await serviceDiscoverya(productId)
    console.log(resu)
    const url = new URL(
      process.env.REACT_APP_REVIEWS_API_URL+'/api/reviews/'+productId
    );

    let headers = {
      "X-Authorization": process.env.REACT_APP_CHEC_SECRET_KEY,
      "Accept": "application/json",
      "Content-Type": "application/json",
    };

    await fetch(url, {
      method: "GET",
      headers: headers,
    })
    .then(response => response.json())
    .then(json => {
        console.log(json)
        setReviews(json)
    });
  }

  if(reviews==undefined )return <Loading/>
  return (
    <Grid container justify="center" spacing={1} >
    {reviews.map((review, idx) => (
    <Grid item key={idx} xs={12} sm={6} md={4} lg={3}>
    <Card elevation={1} sx={{ maxWidth: 345 }}>
      <CardHeader
        avatar={
          <Avatar sx={{ bgcolor: red[500] }} aria-label="recipe">
          </Avatar>
        }
        title={review.title}
        subheader={review.date}
      />
      {/*<Rating name="size-small" defaultValue={Math.floor(Math.random() * (5 - 1 + 1) + 1)} size="small" readOnly />*/}
      <Rating name="size-small" defaultValue={review.stars} size="small" readOnly />
      <CardContent>
        <Typography variant="body2" color="text.secondary">
          {review.content}
        </Typography>
      </CardContent>
      {/* 
      <CardActions disableSpacing>
        <IconButton aria-label="add to favorites">
          <FavoriteIcon />
        </IconButton>
        <IconButton aria-label="share">
          <ShareIcon />
        </IconButton>
      </CardActions>
      */}
    </Card>
    </Grid>
    ))}
    </Grid>
  );
}