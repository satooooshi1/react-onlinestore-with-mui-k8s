import React from "react";
import { useForm, Controller } from "react-hook-form";
import { TextField, Checkbox } from "@mui/material"

import InputLabel from '@mui/material/InputLabel';
import MenuItem from '@mui/material/MenuItem';
import FormControl from '@mui/material/FormControl';
import Select from '@mui/material/Select';

export default function App() {
  const { register, handleSubmit, control, reset, formState:{ errors } } = useForm({
    defaultValues: {
      checkbox: false,
    }
  });
  const onSubmit = data => console.log(data);

  return (
    <form onSubmit={handleSubmit(onSubmit)}>
      <Controller
        name="textfield"
        control={control}
        rules={{ required: true }}
        render={({ field }) => <TextField {...field} />}
      />
      <TextField
      label="example1"
      fullWidth
      name="example1"
      inputRef={register({
        required: "required!"
      })}
      error={Boolean(errors.example1)}
      helperText={errors.example1 && errors.example1.message}
    />
      <Controller
        name="checkbox"
        control={control}
        rules={{ required: true }}
        render={({ field }) => <Checkbox {...field} />}
      />
      <input type="submit" />
    </form>
  );
}